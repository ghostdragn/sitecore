﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Analytics.Configuration;
using Sitecore.Analytics.Pipelines.CreateVisits;
using Sitecore.Diagnostics;

namespace PING.Sitecore.Pipeline.HttpContextSample.Pipelines.CreateVisits
{
	public class UpdateGeoIpData : CreateVisitProcessor
	{
		public override void Process(CreateVisitArgs args)
		{
			Assert.ArgumentNotNull((object) args, nameof(args));

			var geo = new MaxMindData();
			try
			{
				var maxMind = new MaxMindLookupService();
				geo = maxMind.Process(String.Join(".", args.Visit.Ip.ToArray()));
				args.Visit.PostalCode = geo.PostCode;
				args.Visit.Country = geo.Country;
				args.Visit.Latitude = geo.Latitude;
				args.Visit.Longitude = geo.Longitude;

				LogAll(args, geo, null);

				args.Visit.UpdateGeoIpData();
			}
			catch (Exception ex)
			{
				LogAll(args, geo, ex);
			}
		}

		private void LogAll(CreateVisitArgs args, MaxMindData city, Exception ex)
		{
			var debugMode = args.Request.QueryString["sc_geo_trace"];

			var requestHttpHeader = AnalyticsSettings.ForwardedRequestHttpHeader;
			var header = args.Request.Headers[requestHttpHeader];

			var xforwardfor = $"[X-Forwarded-For] ip range: {header}";

			var Output = new StringBuilder();

			Output.AppendLine(xforwardfor);
			Output.AppendLine($"Using {String.Join(".", args.Visit.Ip.ToArray())} for geo Ip searches");

			if (city != null)
			{
				Output.AppendLine($"Country Name: {city.Country}");

				Output.AppendLine($"Post Code: {city.PostCode}");

				Output.AppendLine($"Latitude: {city.Latitude.ToString()}");
				Output.AppendLine($"Longitude: {city.Longitude.ToString()}");
			}

			if (!string.IsNullOrEmpty(debugMode) && debugMode == "1")
			{
				Log.Info(Output.ToString(), this);
			}

			if (ex != null)
			{
				Log.Error($"UpdateGeoIpData", ex, this);
			}


		}
	}

}
