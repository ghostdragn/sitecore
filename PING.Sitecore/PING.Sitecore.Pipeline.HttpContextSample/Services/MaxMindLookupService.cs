﻿using System;
using MaxMind.GeoIP2;
using MaxMind.GeoIP2.Responses;
using Sitecore.Diagnostics;

public class MaxMindData
{
	public MaxMindData()
	{
		PostCode = "Unknown";
		Country = "Unknown";
		Latitude = Longitude = 0;
	}
	public MaxMindData(CityResponse reponse)
	{
		PostCode = reponse.Postal.Code;
		Country = reponse.Country.Name;
		Latitude = reponse.Location.Latitude ?? 0;
		Longitude = reponse.Location.Longitude ?? 0;
	}
	public string PostCode { get; set; }

	public string Country { get; set; }

	public double Latitude { get; set; }

	public double Longitude { get; set; }
}

public class MaxMindLookupService
{
	public MaxMindLookupService()
	{
	}

	public MaxMindData Process(string ipAddress)
	{
		Assert.ArgumentNotNullOrEmpty(ipAddress, nameof(ipAddress));
		var path = System.AppDomain.CurrentDomain.BaseDirectory + "\\App_Data\\" + "GeoLite2-City.mmdb";

		if (System.IO.File.Exists(path))
		{
			try
			{
				using (var reader = new DatabaseReader(path))
				{
					reader.TryCity(ipAddress, out var city);
					if (city == null) { return new MaxMindData(); }

					return new MaxMindData(city);
				}

			}
			catch (Exception ex)
			{
				Log.Error($"An error occurred during the max mind geo ip search for ip: {ipAddress}", ex, this);
			}
		}
		else
		{
			Log.Error("The max mind geoip city database was not found.", this);
		}

		return new MaxMindData();
	}


}