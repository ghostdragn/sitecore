﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PING.Sitecore.Pipeline.HttpContextSample.Tests
{
	[TestClass]
	public class LookupTests
	{
		[TestInitialize]
		public void Startup()
		{
		}
		[TestMethod]
		public void Localhost()
		{
			var d = new MaxMindLookupService();
			var result = d.Process("127.0.0.1");

			Assert.AreEqual(result.PostCode, "Unknown");
		}

		[TestMethod]
		public void NonLocalhost()
		{
			var d = new MaxMindLookupService();
			var result = d.Process("43.252.255.130");

			Assert.AreNotEqual(result.PostCode, "Unknown");
		}

		[TestMethod]
		public void EmtpyIp()
		{
			var d = new MaxMindLookupService();
			Assert.ThrowsException<ArgumentException>(() => d.Process(""));
		}
	}
}
